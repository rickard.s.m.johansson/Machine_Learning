%% Problem 2.1
clc 
clf

% Initialize all constants
mu = [1,1];
sigma = [0.1, -0.05;
        -0.05, 0.2];
invSigma = inv(sigma);
nbrOfPoints = 1000;

% Generate multivariate points
points = mvnrnd(mu,sigma,nbrOfPoints);


fun = @(x,r) ((x-mu) * invSigma * (x-mu)')/2 - r;

syms x y
hold on
for r = 1:3
    h = ezplot((([x,y]-mu) * invSigma * ([x,y]-mu)')/2 - r == 0, [-1,3])
    set(h,'Color','r','LineWidth',2);
end

funcVal = zeros(nbrOfPoints,1);
for i = 1:nbrOfPoints
   if fun(points(i,:),3) <= 0
       funcVal(i) = 1;
   end
end
% Plot all points inside blue
indices = find(funcVal);
scatter(points(indices,1),points(indices,2),'blue')
% Plot all points inside black
indices = find(funcVal==0);
scatter(points(indices,1),points(indices,2),'black')

title(sprintf('Points outside: %d',length(indices)));
set(gca,'fontsize',14)
print('Problem2.1.eps','-depsc')




%% Problem 2.2

clc
clf
clear all
format short

X = dlmread('dataset0.txt');
cX = cov(X);
rX = corrcov(cX);

Y = zeros(size(X));
for i = 1:length(X(1,:))
    Y(:,i) = X(:,i)/max(X(:,i));
end


cY = cov(Y);
rY = corrcov(cY);
imagesc(rX)
set(gca,'fontsize',14)
title('Correlation X')
print('Problem2.2-rX.eps','-depsc')
figure
imagesc(rY)
set(gca,'fontsize',14)
title('Correlation Y')
print('Problem2.2-rY.eps','-depsc')
figure 
imagesc(cX)
set(gca,'fontsize',14)
title('Covariance X')
print('Problem2.2-cX.eps','-depsc')
figure
imagesc(cY)
set(gca,'fontsize',14)
title('Covariance Y')
print('Problem2.2-cY.eps','-depsc')
figure 

rY = round(rY,10);
value = min(min(rY));
[~, indices] = find(rY == value);

scatter(Y(:,indices(1)), Y(:,indices(2)))
set(gca,'fontsize',14)
title(sprintf('Correlation value: %f and indices: %d,%d', value,indices))
print('Problem2.2.eps','-depsc')


