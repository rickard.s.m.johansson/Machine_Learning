% -------------------------------------------------------------------
%
% This file is part of an exercise in the Machine Learning course of
% Chalmers University of Technology
%
% Author of exercise: Fredrik Johansson (2013)
%
%
% ldaGibbs(data,nTopics,alpha,eta,nIt,nReadIt,
%          nBurnIn, nWords)
% trains the LDA model based on wordcounts in data using nTopics
% topics.
%
% data      A cell-array where each element is a sparse representation
%           of the word count for a single document. Each cell has two
%           fields, id (representing word ids) and cnt (counts)
% nTopics   The number of topics to be used
% alpha     The parameter alpha
% eta       The parameter eta
% nIt       The number of Gibbs iterations to perform
% nReadIt   The number of iterations between parameter readouts
% nBurnIn   The number of iterations making up the burn-in
% nWords    The number of words in the vocabulary
%
% -------------------------------------------------------------------

function [beta, theta] = ldaGibbs(data, nTopics, alpha, eta, ...
    nIt, nReadIt, nBurnIn, nWords)

% Show progress
h = waitbar(0,'Progress');

% --- Variable declaration
nDocuments = length(data);
beta = zeros(nTopics,nWords);
theta = zeros(nDocuments,nTopics);

% ------- BELOW, YOU SHOULD IMPLEMENT THE GIBBS SAMPLER -------------
% ------- AND OUTPUT beta AND theta.                     -------------


% --- Initialization

nReadOuts = 0;

z = zeros(nDocuments, nWords);      % Topic associated with each occurance of a word
ndk = zeros(nDocuments,nTopics);    % Number of topic counts for each document
nkw = zeros(nTopics,nWords);        % Number of occurances of a word in a topic
nk = zeros(nTopics,1);                    % Total count of words in a topic
for doc = 1:nDocuments
    counts = cumsum(data{doc}.cnt);
    wordIndex = 1;
    for  i = 1:counts(end)
        word = data{doc}.id(wordIndex);
        if (i == counts(wordIndex))
            wordIndex = wordIndex + 1;
        end
        
        z(doc,i) = floor((nTopics)*rand) + 1; % Uniform sample topic
        ndk(doc,z(doc,i)) = ndk(doc,z(doc,i)) + 1;
        nkw(z(doc,i),word) = nkw(z(doc,i),word) + 1;
        nk(z(doc,i)) = nk(z(doc,i)) + 1;
    end;
end;


% --- Inference, sampling Z
for iter = 1:nIt
        
    % Update posterior
    for doc = 1:nDocuments
        counts = cumsum(data{doc}.cnt);
        wordIndex = 1;
        for  i = 1:counts(end)
            word = data{doc}.id(wordIndex);
            if (i == counts(wordIndex))
                wordIndex = wordIndex + 1;
            end
            
            topic = z(doc,i);
            
            % Don't include current word
            ndk(doc,topic) = ndk(doc,topic) - 1;
            nkw(topic,word) = nkw(topic,word) - 1;
            nk(topic) = nk(topic) - 1;
            
            % Calculate posterior
            pz = (nkw(:,word)+eta)./(nk+nWords*eta) ...
                .* (ndk(doc,:)'+alpha)/(nTopics*alpha + sum(ndk(doc,:)));
            pz = pz/sum(pz);
            
            
            % Draw a sample from the posterior
            pz = cumsum(pz);
            temp = find(pz > rand);
            topic = temp(1);
            z(doc,i) = topic;
            
            % Update counters with new sample
            ndk(doc,topic) = ndk(doc,topic) + 1;
            nkw(topic,word) = nkw(topic,word) + 1;
            nk(topic) = nk(topic) + 1;
        end;
    end;
    
    
    
    if (iter >= nBurnIn && mod(iter,nReadIt) == 0)
        nReadOuts = nReadOuts + 1;
        
        % --- Estimate parameters, beta, theta
        for topic = 1:nTopics
            beta(topic,:) = beta(topic,:) + (nkw(topic,:)+eta)/(nk(topic)+nTopics*eta);
        end
        
        for doc = 1:nDocuments
            theta(doc,:) = theta(doc,:) + (ndk(doc,:) + alpha)/(nTopics*alpha + sum(ndk(doc,:)));
        end
        
        
    end
    
    % Update progress
    waitbar(iter/nIt,h);
    
end

beta = beta/nReadOuts;
theta = theta/nReadOuts;
close(h)

end





