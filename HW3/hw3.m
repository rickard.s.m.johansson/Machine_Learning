clc

learning_rates = [0.002, 0.01, 0.05, 0.2, 1.0, 5.0, 20.0];
momentums = [0, 0.9];
for i = 1:length(momentums)
    momentum = momentums(i);
    for j = 1:length(learning_rates)
        learning_rate = learning_rates(j)
        net(0, 10, 70, learning_rate, momentum, false, 4);
    end
end


%% Task 2.4
clc

% a)
%net(0, 200, 1000, 0.35, 0.9, false, 100);

% b)
%  net(0, 200, 1000, 0.35, 0.9, true, 100);

% c)
% weigth_decays = [10, 1, 0.0001, 0.001, 5];
% for i = 1:length(weigth_decays)
%     wd = weigth_decays(i);
%     net(wd, 200, 1000, 0.35, 0.9, false, 100);
% end

% d)
% hidden_neurons = [10, 30, 100, 130, 200];
% for i = 1:length(hidden_neurons)
%     neurons = hidden_neurons(i);
%     net(0, neurons, 1000, 0.35, 0.9, false, 100);
% end

% e)
% hidden_neurons = [18, 37, 83, 113, 236];
% for i = 1:length(hidden_neurons)
%     neurons = hidden_neurons(i);
%     net(0, neurons, 1000, 0.35, 0.9, true, 100);
% end


% f
% net(0.001, 37, 1000, 0.2, 0.9, true, 100);
net(0.0007, 37, 1000, 0.35, 0.9, true, 100);





