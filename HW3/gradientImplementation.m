%% TODO - Write code here ---------------

error = (model.hid_to_class'*(class_prob-data.targets).* ...
    ((logistic(hid_input)-logistic(hid_input).^2)))*data.inputs';
n = length(data.inputs(1,:));

class_part = error/n;

wd_part =  model.input_to_hid * wd_coefficient;
total_grad = class_part + wd_part;

res.input_to_hid = total_grad;

error = (class_prob-data.targets)*hid_output';
class_part = error/n;
wd_part =  model.hid_to_class * wd_coefficient;
total_grad = class_part + wd_part;

res.hid_to_class = total_grad;

% ---------------------------------------