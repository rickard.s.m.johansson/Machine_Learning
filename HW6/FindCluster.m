function classes = FindCluster(data, mu, method, ZNK)
    if nargin < 3
        method = 'linear'
    end
    if nargin < 4
        ZNK = 0;
    end       
    
    observations = length(data(:,1));
    classes = zeros(observations,1);
    if strcmp(method,'linear')
        for i = 1:observations
            [~, b] = min(arrayfun(@(x) norm(mu(x,:) - data(i,:)), 1:length(mu(:,1))));
            classes(i) = b;
        end;
    elseif strcmp(method,'rbf') || strcmp(method,'RBF')
        sigma = 0.2;
        K = @(x,y) exp(-norm([x-y],2)^2/(2*sigma^2));
        Nk = sum(ZNK);
        c = 0;
        for m = 1:observations
            for l = 1:observations
                c = c + ZNK(m,:).*ZNK(l,:).*(K(data(m,:),data(l,:)));
            end;
        end;
        c = c./(Nk.^2);
        for i = 1:observations
            a = K(data(i,:),data(i,:));
            b = 0;
            for j = 1:observations
                b = b + ZNK(j,:).*K(data(i,:),data(j,:));
            end;
            b = b.*2./Nk;
            [~, class] = min(a - b + c);
            classes(i) = class;
        end;
    else
        disp('Method not implemented')
    end
    
    
    
end

