function [s, dist] = invGamma( beta, alpha)
% Returns the inverse-gamma disturbution for a given alpha and beta
s = linspace(0.001,4,1000);
dist= zeros(1,1000);

a = alpha-1;
suma = sum(log(1:a));

for i=1:1000
    dist(i)= exp(((alpha*log(beta))-(suma)-((alpha+1)*log(s(i)))-(beta/s(i)))); % log-sum-exp trick
end 
end

