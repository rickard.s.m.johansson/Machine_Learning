function [P1, P2, Ytest]=sph_bayes(Xtest, mu1, mu2, var1, var2) 
    % omitted some constant factors + P(t_new = +-1) since this was equal
    % for both classes
    prob = @(mu, var, Xtest) 1/sqrt(var)* ...
            exp(-1/(2*var)*norm(Xtest-mu,2));

    P1 = prob(mu1,var1,Xtest)/(prob(mu1,var1,Xtest) + prob(mu2,var2,Xtest));
    P2 = 1-P1;
    
    if P1 > P2
        Ytest = 1;
    else
        Ytest = -1;
    end
end

