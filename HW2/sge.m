function [mu, sigma] = sge(x)
%
% SGE Mean and variance estimator for spherical Gaussian distribution
%
% x : Data matrix of size n x d where each row represents a
% d-dimensional data point e.g.
% x = [2 1;
% 3 7;
% 4 5 ] is a dataset having 3 samples each
% having two co-ordinates.
%
% mu : Estimated mean of the dataset [mu_1 mu_2 ... mu_d]
% sigma : Estimated standard deviation of the dataset (number)
%
len = length(x(:,1));

mu = sum(x)/len;

mask = repmat(mu,len,1);
temp = sum((x - mask).^2)/len;
var = sum(temp)/length(temp);
sigma = sqrt(var);
end