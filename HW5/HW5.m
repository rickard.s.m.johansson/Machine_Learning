%%%%%%%%%%%%%%%%%% HW5 %%%%%%%%%%%%%%%%%%%%%
%% 1.1
clear all
x1Plus = [2 4 4];
x2Plus = [2 4 0];
x1Minus = [0 2 0];
x2Minus = [0 0 2];
xgamma = [2 1.5];
ygamma = [2 1.5];
hold on
plot(x1Plus, x2Plus, '+b');
plot(x1Minus, x2Minus, 'r.');
syms x y
ezplot(x+y+-3==0);
plot(xgamma, ygamma,'--')
axis([-2 5 -2 5]);
ylabel('x2');
xlabel('x1');
h = legend('1', '-1', 'Hyperplane', '$\gamma$');
set(h,'Interpreter','latex')
%% Task 1.2 ab
X = [2,2;
     4,4;
     4,0;
     0,0;
     2,0;
     0,2;];
 Y = [1,1,1,-1,-1,-1]';
 
 
 l = length(X(:,1));
 H = eye(3);
 f = zeros(3,1);
 
 A = -diag(Y)*[X ones(l,1)]
 c = -ones(l,1);
 
 quadprog(H,f,A,c)
 
 %% 1.2 cd
 H = (Y*Y').*(X*X');
 f=-ones(1,length(H));
 A = zeros(1,length(H));
 c = 0;
 Aeq=Y';
 ceq = 0;
 LB=zeros(length(H),1);
 UB=inf*ones(length(H),1);
 
alpha=quadprog(H,f,A,c,Aeq,ceq,LB,UB)
 
w = X'*(alpha.*Y)
b = 1-(X(1,:)*w)
%% 2.1

data = load('d1b.mat');

hold on
svmStruct = svmtrain(data.X, data.Y,'boxconstraint', 1,'autoscale',false);
gscatter(data.X(:,1),data.X(:,2),data.Y,'rb','.+',6);
plot(svmStruct.SupportVectors(:,1),svmStruct.SupportVectors(:,2), 'o');
class = svmclassify(svmStruct,data.X,'ShowPlot',true);
misplaced = svmStruct.GroupNames - class;
for i = 1:length(data.X)
    if misplaced(i) ~= 0
        plot(data.X(i,1),data.X(i,2),'.r','MarkerSize',21);
    end
end
w =  svmStruct.SupportVectors' * svmStruct.Alpha;
syms x y
ezplot(w(1)*x+w(2)*y+svmStruct.Bias==0); %plots hyperplane

legend('-1','1','Support vectors', 'Misplaced','Boundary');
xlabel('x1');
ylabel('x2');
title('Box-constrain = 4')

%% 2.2a
clc

data = load('d2.mat');

hold on
svmStruct = svmtrain(data.X, data.Y,'boxconstraint', 1,'autoscale',false, 'kernel_function', 'linear');
gscatter(data.X(:,1),data.X(:,2),data.Y,'rb','.+',6); %plot data
class = svmclassify(svmStruct,data.X,'ShowPlot',true);
misplaced = svmStruct.GroupNames - class;
for i = 1:length(data.X)
    if misplaced(i) ~= 0
        plot(data.X(i,1),data.X(i,2),'or');
    end
end
w =  svmStruct.SupportVectors' * svmStruct.Alpha;

legend('-1','1', 'Misplaced');
xlabel('x1');
ylabel('x2')

%% 2.2b
clc
data = load('d2.mat');
tic; %timer starter
indices = crossvalind('Kfold', length(data.Y), 5); 
error = 0;
for i = 1:5
    test = (indices == i); train = ~test;
    trainInd = 1;
    testInd = 1;
    for j=1:length(data.Y) %devides the folds into vectors
        if(train(j))
            trainX(trainInd,1:2) = data.X(j,1:2);
            trainY(trainInd) = data.Y(j);
            trainInd = trainInd + 1;
        else
            testX(testInd,1:2) = data.X(j,1:2);
            testY(testInd) = data.Y(j);
            testInd = testInd + 1;
        end
    end
    %just change kernel and method in svmtrain for the one to test
    svmStruct = svmtrain(trainX, trainY, 'boxconstraint', 1, 'autoscale', false, 'kernel_function', 'linear', 'method', 'QP');
    group = svmclassify(svmStruct, testX);
    error = error + sum(abs(testY' - group))/2;
end
errorRate = error/length(data.Y)
toc; %timer stop

%% 2.2c
clc
data = load('d2.mat');

figure;
hold on
gscatter(data.X(:,1),data.X(:,2),data.Y,'rb','.+',6);%plot data

% Compute the scores over a grid
d = 0.02; % Step size of the grid
[x1Grid,x2Grid] = meshgrid(min(data.X(:,1)):d:max(data.X(:,1)),...
    min(data.X(:,2)):d:max(data.X(:,2)));
xGrid = [x1Grid(:),x2Grid(:)];        

SVMModel2 = fitcsvm(data.X,data.Y,'KernelFunction','rbf','Standardize',true);
[~,scores2] = predict(SVMModel2,xGrid);

title('Decision Boundary for RBF kernel')
contour(x1Grid,x2Grid,reshape(scores2(:,2),size(x1Grid)),[0 0],'k'); %plot boundary
legend('-1','1','Boundary');
xlabel('x1');
ylabel('x2')
hold off
